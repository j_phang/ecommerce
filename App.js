import React, { Component } from 'react'
import Home from './src/pages/Home'
import Login from './src/pages/Login'
import Register from './src/pages/Register'
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { Root } from 'native-base';
import Profile from './src/pages/Profile';
import Stored from './src/pages/Store'
import Details from './src/pages/Details'
import { Provider } from 'react-redux';
import configureStore from './store';
import Tambah_Product from './src/pages/Tambah_Product';
import Cart from './src/pages/Cart';
const store = configureStore();

class HomeScreen extends Component {
  render() {
    return <Home />
  }
}

class Profiles extends Component {
  render() {
    return <Profile />
  }
}

class Registers extends Component {
  render() {
    return <Register />
  }
}

class Store extends Component {
  render() {
    return <Stored />
  }
}

class Detailed extends Component {
  render() {
    return <Details />
  }
}

class Tambah_Products extends Component {
  render() {
    return <Tambah_Product />
  }
}

class Carts extends Component {
  render(){
    return <Cart />
  }
}

const StackNav = createStackNavigator(
  {
    Home: {
      screen: HomeScreen
    },
    Profile: {
      screen: Profiles
    },
    Register: {
      screen: Registers
    },
    Store: {
      screen: Store
    },
    Details: {
      screen: Detailed
    },
    Tambah_Product: {
      screen: Tambah_Products
    },
    Cart: {
      screen: Carts
    }
  },
  {
    initialRouteName: 'Home',
    headerMode: 'none'
  }
)

const AppContainer = createAppContainer(StackNav)

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <AppContainer />
      </Provider>
    )
  }
}