import { FETCH_ID } from '../type/BarangType'

const initialState = {
    barang: 'kosong cuk'
}

export default (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ID:
            return {
                ...state,
                barang: action.payload
            };
        default:
            return state;
    }
};