import { FETCH_USER } from '../type/UserType';

const initialState = {
    users:{
        profiles:[
            null
        ]
    }
}

export default (state = initialState, action) => {
    switch (action.type) {
        case FETCH_USER:
            return {
                ...state,
                users: action.payload
            };
        default:
            return state;
    }
};