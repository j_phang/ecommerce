import { combineReducers } from "redux";
import auth from './AuthReducer';
import user from './UserReducer'
import barang from './BarangReducer'

const IndexReducer = combineReducers({
    auth: auth,
    user: user,
    barang: barang
})

export default IndexReducer