import { FETCH_USER } from '../type/UserType'

export const user = data => {
    return {
        type: FETCH_USER,
        payload: data
    }
}