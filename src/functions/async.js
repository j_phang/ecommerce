import AsyncStorage from "@react-native-community/async-storage"

module.exports = {
    setData: async (nama, data) => {
        try {
            await AsyncStorage.setItem(`@${nama}`, data)
            console.log(`berhasil masukin data!`)
        } catch (e) {
            console.log(e)
        }
    },

    getData: async (nama) => {
        try {
            const data = await AsyncStorage.getItem(`@${nama}`)
            if (data !== null) {
                // console.log(data)
                return data
            } else {
                console.log(data)
            }
        } catch (e) {
            console.log(e)
        }
    }
}
