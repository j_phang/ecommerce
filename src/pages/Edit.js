import React, { Component } from 'react'
import { Text, View, ToastAndroid } from 'react-native'
import { withNavigation } from 'react-navigation'
import { Container, Item, Label, Input, Button } from 'native-base'
import { connect } from 'react-redux'
import { auth } from '../redux/action/AuthAction';
import { user } from '../redux/action/UserAction'
import { barang } from '../redux/action/BarangAction'
import * as functs from '../functions/async'
import Axios from 'axios'

const mapStateToProps = state => ({
    auth: state.auth,
    user: state.user,
    barang: state.barang
});

const mapDispatchToProps = (dispatch) => {
    return {
        FetchToken: token => dispatch(auth(token)),
        FetchUser: users => dispatch(user(users)),
        Barang: id => dispatch(barang(id))
    };
};

class Edit extends Component {
    constructor(props) {
        super(props)
        this.state = {
            name: null,
            tags: null
        }
    }

    Change = async () => {
        const token = await functs.getData('token')
        console.log(token)
        try {
            const api = async (objParam) => await Axios.put(
                `http://basic-commerce.herokuapp.com/api/profile/update`,
                objParam, {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `jwt ${token}`
                }
            }
            )

            api({
                name: this.state.name,
                tags: this.state.tags
            })
                .then(res => {
                    console.log(res)
                    ToastAndroid.show('Data Updated!', ToastAndroid.SHORT)
                    this.setState({
                        name: null,
                        tags: null
                    })
                })
                .catch(err => {
                    console.log(err)
                })
        } catch (err) {
            console.log(err)
        }
    }

    render() {
        return (
            <Container>
                <Item floatingLabel>
                    <Label>
                        Name : {this.props.auth.token.name}
                    </Label>
                    <Input
                        value={this.state.name}
                        onChangeText={(text) => {
                            this.setState({
                                name: text
                            })
                        }}
                    />
                </Item>
                <Item floatingLabel>
                    <Label>
                        Tags : {this.props.auth.token.tags}
                    </Label>
                    <Input
                        value={this.state.tags}
                        onChangeText={(text) => {
                            this.setState({
                                tags: text
                            })
                        }}
                    />
                </Item>
                <Button
                    onPress={
                        () => {
                            this.Change()
                        }
                    }
                >
                </Button>
            </Container>
        )
    }
}

export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(Edit))