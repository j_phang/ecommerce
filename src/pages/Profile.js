import React, { Component } from 'react'
import { withNavigation } from 'react-navigation'
import Login from './Login';
import { Container, Text, Button } from 'native-base';
import Profiles from './Profiles';
import * as functs from '../functions/async'
import { ToastAndroid } from 'react-native'
import Axios from 'axios';
import Store from './Store';
import { auth } from '../redux/action/AuthAction';
import { connect } from 'react-redux'

const mapStateToProps = state => ({
    auth: state.auth
});

const mapDispatchToProps = dispatch => {
    return {
        FetchInfo: infos => dispatch(auth(infos))
    };
};

class Profile extends Component {
    constructor(props) {
        super(props)
        this.state = {
            token: '',
            username: '',
            data: ''
        }
    }

    componentDidMount() {
        this.fetchToken()
    }

    fetchToken = async () => {
        const data = await functs.getData(`token`)
        this.setState({
            token: data
        })
    }

    render() {
        if (this.state.token !== 'null') {
            return <Profiles />
        } else {
            return <Login />
        }
    }
}

export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(Profile))