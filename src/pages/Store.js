import React, { Component } from 'react'
import { View } from 'react-native'
import * as functs from '../functions/async'
import { withNavigation } from 'react-navigation'
import Header_Login from '../component/Header_Login'
import { Text, Container, Button, Card, CardItem, Left, Thumbnail, Body, Right, Tabs, Tab, Fab, Icon, Header, Footer, FooterTab, Title, Spinner, Content, Image } from 'native-base'
import { ScrollView } from 'react-native-gesture-handler'
import Barang from './Barang'
import Login from './Login'
import Maaf from './Maaf'
import { connect } from 'react-redux'
import { auth } from '../redux/action/AuthAction';
import { user } from '../redux/action/UserAction'

const mapStateToProps = state => ({
    auth: state.auth,
    user: state.user
});

const mapDispatchToProps = (dispatch) => {
    return {
        FetchToken: token => dispatch(auth(token)),
        FetchUser: users => dispatch(user(users))
    };
};

class Stored extends Component {
    constructor(props) {
        super(props)
        this.state = {
            token: '',
            username: '',
            active: false,
            token: ''
        }
    }

    componentDidMount() {
        this.token()
    }

    token = async () => {
        const tokens = await functs.getData(`token`)
        this.setState({
            token: tokens
        })
    }

    logout = () => {
        functs.setData('token', 'null')
        functs.setData('username', 'null')
        this.props.navigation.navigate('Home')
    }

    render() {

        if (this.state.token == 'null') {
            return <Login />
        } else if (this.props.auth.token.role !== 'Merchant') {
            return <Maaf />
        } else if (this.props.auth.token.role == 'Merchant') {
            return (
                <Container>
                    <Header
                        transparent
                    >
                        <Left
                            style={{
                                display: 'flex',
                                flexDirection: 'row'
                            }}
                        >
                            <Icon
                                type="MaterialIcons"
                                name="arrow-back"
                                onPress={
                                    () => {
                                        this.props.navigation.pop()
                                    }
                                }
                            />
                        </Left>
                        <Body>
                            <Title
                                style={{
                                    color: 'black'
                                }}
                            >
                                {this.props.auth.token.name}
                            </Title>
                        </Body>
                        <Right>
                            <Button
                                full
                                onPress={() => this.logout()}
                            >
                                <Text>Logout</Text>
                            </Button>
                            <Button
                                full
                                onPress={
                                    () => this.props.navigation.navigate('Tambah_Product')
                                }
                            >
                                <Icon
                                    name='add'
                                />
                            </Button>
                        </Right>
                    </Header >
                    <Body>
                        <View>
                            <Tabs>
                                <Tab heading="Barang">
                                    <Barang />
                                </Tab>
                                <Tab heading="Transaksi">
                                    <Text>Orderan yang belum siap</Text>
                                </Tab>
                            </Tabs>
                        </View>
                    </Body>
                </Container >
            )
        } else {
            return (
                <Container>
                    <Header_Login judul={'Profile'} />
                    <View>
                        <Spinner />
                    </View>
                </Container>
            )
        }
    }
}

export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(Stored))