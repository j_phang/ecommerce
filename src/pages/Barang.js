import React, { Component } from 'react'
import { ToastAndroid, Dimensions, Modal, Alert } from 'react-native'
import { View, Text, Container, CardItem, Card, Left, Thumbnail, Body, Right, Fab, Icon, Button, Footer, FooterTab, Content, Image, Spinner } from 'native-base'
import { withNavigation } from 'react-navigation'
import { connect } from 'react-redux'
import { auth } from '../redux/action/AuthAction';
import { user } from '../redux/action/UserAction'
import * as functs from '../functions/async'
import Axios from 'axios'

const mapStateToProps = state => ({
    auth: state.auth,
    user: state.user
});

const mapDispatchToProps = (dispatch) => {
    return {
        FetchToken: token => dispatch(auth(token)),
        FetchUser: users => dispatch(user(users))
    };
};

class Barang extends Component {
    constructor(props) {
        super(props)
        this.state = {
            active: false
        }
    }

    getProfile = async (id) => {
        const token = await functs.getData(`token`)
        try {
            const getProfile = async () => Axios.get(
                `https://basic-commerce.herokuapp.com/api/profile/show/${id}`,
                {
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `jwt ${token}`
                    }
                }
            )

            getProfile()
                .then(
                    res => {
                        console.log(`ini getProfile`)
                        this.props.FetchToken(res.data.result)
                        this.setState({
                            active: false
                        })
                    }
                )
                .catch(
                    err => {
                        console.log(err)
                    }
                )
        } catch (err) {
            console.log(err)
        }
    }

    deleteItem = async (id) => {
        const token = await functs.getData('token')
        try {
            const APIdel = async () => Axios.delete(
                `http://basic-commerce.herokuapp.com/api/product/delete/${id}`,
                {
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `jwt ${token}`
                    }
                }
            )

            APIdel()
                .then(res => {
                    // this.getProfile(this.props.user.users.profiles[0])
                    console.log(res)
                    this.setState({
                        active: false
                    })
                })
                .catch(err => {
                    console.log(err)
                })
        } catch (err) {
            console.log(err)
        }
    }

    render() {
        // console.log(this.props.auth.token.products)
        const looping = this.props.auth.token.products.map(value => {
            return (
                <Card
                    key={value._id}
                >
                    <CardItem>
                        <Left>
                            {/* <Image
                                source={{ uri: value.preview }}
                                style={{
                                    height: 100, width: null, flex: 1
                                }}
                            /> */}
                        </Left>
                        <Body>
                            <Text>{value.name}</Text>
                            <Text note>{value.price}</Text>
                        </Body>
                        <Right>
                            <Button
                                transparent
                                onPress={
                                    () => {
                                        this.deleteItem(value._id)
                                        this.setState({
                                            active: true
                                        })
                                    }
                                }
                            >
                                <Icon
                                    type='FontAwesome'
                                    name='trash'
                                />
                            </Button>
                        </Right>
                    </CardItem>
                </Card>
            )
        })
        return (
            <View
                style={{
                    // flex: 1
                    height: Dimensions.get('window').height
                }}
            >
                <Content>
                    {looping}
                </Content>
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.active}
                    onRequestClose={
                        () => Alert.alert(`Hapus Berhasil`)
                    }
                >
                    <View
                        style={{
                            backgroundColor: 'rgba(0,0,0,0.4)',
                            display: 'flex',
                            flex: 1,
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}
                    >
                        <Spinner />
                    </View>
                </Modal>
            </View>
        )
    }
}

export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(Barang))