import React, { Component } from 'react'
import { Text, View, Modal, ToastAndroid } from 'react-native'
import { withNavigation } from 'react-navigation'
import * as functs from '../functions/async'
import { connect } from 'react-redux'
import { auth } from '../redux/action/AuthAction';
import { user } from '../redux/action/UserAction'
import ImagePicker from 'react-native-image-picker'
import { Container, Content, Item, Header, Spinner, Card, CardItem, Right, Left, Body, Button, Icon, Footer, Image } from 'native-base'
import Header_Login from '../component/Header_Login'
import Axios from 'axios'

const mapStateToProps = state => ({
    auth: state.auth,
    user: state.user,
    barang: state.barang
});

const mapDispatchToProps = (dispatch) => {
    return {
        FetchToken: token => dispatch(auth(token)),
        FetchUser: users => dispatch(user(users)),
        Barang: id => dispatch(barang(id))
    };
};

class Cart extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [],
            loop: [
                {
                    products: "wow"
                }
            ],
            loading: true,
            details: [
                {
                    name: 'barang',

                }
            ],
            active: false
        }
    }

    componentDidMount() {
        // this.orders()
        setInterval(this.orders, 1000)
    }

    orders = async () => {
        const token = await functs.getData('token')
        try {
            const getOrder = async () => await Axios.get(
                `http://basic-commerce.herokuapp.com/api/order/detail/${this.props.auth.token.orders}`,
                {
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `jwt ${token}`
                    }
                }
            )
            getOrder()
                .then(res => {
                    console.log(res.data.result.details)
                    if (Array.isArray(this.state.details)) {
                        res.data.result.details.map((value) => {
                            this.fetchData(value.products)
                        })
                    }
                    console.log(this.state.details)
                    this.setState({
                        data: res.data.result,
                        loop: res.data.result.details,
                        loading: false
                    })
                })
                .catch(err => {
                    ToastAndroid.show('Maaf Cart sedang dibuat!', ToastAndroid.SHORT)
                    console.log(err)
                    this.createCart()
                })
        } catch (err) {
            console.log(err)
        }
    }

    createCart = async () => {
        const token = await functs.getData('token')
        try {
            const api = async () => await Axios.post(
                `http://basic-commerce.herokuapp.com/api/order/create`,
                null,
                {
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `jwt ${token}`
                    }
                }
            )

            api()
                .then(res => {
                    ToastAndroid.show(`Cart Berhasil dibuat! Silahkan masukkan Barang`, ToastAndroid.SHORT)
                })
                .catch(err => {
                    console.log(err)
                })
        } catch (err) {
            console.log(err)
        }
    }

    fetchData = async (id) => {
        try {
            const dash = async () => await Axios.get(
                `http://basic-commerce.herokuapp.com/api/product/detail/${id}`
            )

            dash()
                .then(res => {
                    this.state.details.push(res.data.result)
                })
                .catch(err => {
                    console.log(err)
                })

        } catch (err) {
            console.log(err)
        }
    }

    deleteProd = async (id) => {
        const token = await functs.getData('token')
        console.log('Manggil Delete')
        try {
            const deletes = async () => Axios.delete(
                `http://basic-commerce.herokuapp.com/api/order/remove-product/${id}`,
                {
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `jwt ${token}`
                    }
                }
            )
            deletes()
                .then(res => {
                    console.log(res)
                    this.setState({
                        active: false
                    })
                })
                .catch(err => {
                    console.log(err)
                })
        } catch (err) {
            console.log(err)
        }
    }

    checkOut = async () => {
        try {
            const token = await functs.getData('token')
            console.log(this.props.auth.token.orders[0])
            console.log(token)
            const getOrder = async () => await Axios.post(
                `http://basic-commerce.herokuapp.com/api/order/checkout/${this.props.auth.token.orders[0]}`,
                null,
                {
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `jwt ${token}`
                    }
                }
            )
            getOrder()
                .then(res => {
                    console.log(res)
                    this.props.navigation.pop()
                })
                .catch(err => {
                    console.log(err)
                })
        } catch (e) {
            console.log(e)
        }
    }

    render() {
        const loop = this.state.loop.map((value, id) => {
            return (
                <Card
                    key={value._id}
                >
                    <CardItem>
                        <Left>
                            {/* <Image
                                source={{ uri: this.state.details.preview }}
                                style={{
                                    height: 100, width: null, flex: 1
                                }}
                            /> */}
                        </Left>
                        <Body>
                            <Text>{this.state.details[0].name}</Text>
                            <Text note>{this.state.details[0].price}</Text>
                        </Body>
                        <Right>
                            <Button
                                transparent
                                onPress={
                                    () => {
                                        this.deleteProd(value._id)
                                        this.setState({
                                            active: true
                                        })
                                    }
                                }
                            >
                                <Icon
                                    type='FontAwesome'
                                    name='trash'
                                />
                            </Button>
                        </Right>
                    </CardItem>
                </Card>
            )
        })
        if (this.state.loading) {
            return (
                <Container>
                    <Header transparent />
                    <View>
                        <Spinner />
                    </View>
                </Container>
            )
        } else {
            return (
                <Container>
                    <Header_Login judul={'Cart'} />
                    <Content>
                        {loop}
                    </Content>
                    <Footer>
                        <Left>
                            <Text>Total : </Text>
                            <Text>{this.state.data.total}</Text>
                        </Left>
                        <Right>
                            <Button
                                onPress={
                                    () => {
                                        this.checkOut()
                                    }
                                }
                            >
                                <Icon
                                    type='MaterialIcons'
                                    name='done'
                                />
                            </Button>
                        </Right>
                    </Footer>
                    <Modal
                        animationType="fade"
                        transparent={true}
                        visible={this.state.active}
                        onRequestClose={
                            () => Alert.alert(`Hapus Berhasil`)
                        }
                    >
                        <View
                            style={{
                                backgroundColor: 'rgba(0,0,0,0.4)',
                                display: 'flex',
                                flex: 1,
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}
                        >
                            <Spinner />
                        </View>
                    </Modal>
                </Container>
            )
        }
    }
}

export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(Cart))