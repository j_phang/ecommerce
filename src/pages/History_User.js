import React, { Component } from 'react'
import { View } from 'react-native'
import { Text, Container, Card, CardItem, Left, Thumbnail, Body, Right } from 'native-base'

export default class History_User extends Component {
    render() {
        return (
            <Container>
                <Card>
                    <CardItem button onPress={
                        () => {
                            ToastAndroid.show('Harusnya ke menu details', ToastAndroid.SHORT)
                        }
                    }>
                        <Left>
                            <Thumbnail source={require('../bgd/Contoh.png')} />
                            <Body>
                                <Text>Nama Barang</Text>
                                <Text note>Status Pembelian</Text>
                            </Body>
                        </Left>
                        <Right>
                            <Text>Rp. 1.000.000</Text>
                        </Right>
                    </CardItem>
                </Card>
            </Container>
        )
    }
}
