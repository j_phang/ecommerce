import React, { Component } from 'react'
import { ToastAndroid } from 'react-native'
import { withNavigation } from 'react-navigation'
import { View, Text, Container, Button, Thumbnail, Card, Left, Right, Body, CardItem, Tabs, Tab, Footer, Header, Spinner } from 'native-base'
import Header_Login from '../component/Header_Login'
import { ScrollView } from 'react-native-gesture-handler'
import * as functs from '../functions/async'
import History_User from './History_User'
import Maaf from './Maaf'
import { connect } from 'react-redux'
import { auth } from '../redux/action/AuthAction';
import { user } from '../redux/action/UserAction'
import Edit from './Edit'

const mapStateToProps = state => ({
    auth: state.auth,
    user: state.user
});
// redux store in here

const mapDispatchToProps = dispatch => {
    return {
        FetchToken: token => dispatch(auth(token)),
        FetchUser: users => dispatch(user(users))
    };
};

class Profiles extends Component {
    constructor(props) {
        super(props)
        this.state = {
        }
    }

    logout = () => {
        functs.setData('token', 'null')
        functs.setData('username', 'null')
        this.props.navigation.navigate('Home')
    }

    render() {
        console.log(`ini render`)
        console.log(this.props.auth)
        if (this.props.auth.token.role == "Buyer") {
            return (
                <Container>
                    <Header_Login judul={'Profile'} />
                    <View>
                        <ScrollView>
                            <Card>
                                <CardItem button onPress={
                                    () => {
                                        ToastAndroid.show('Harusnya ke menu details', ToastAndroid.SHORT)
                                    }
                                }>
                                    <Left>
                                        <Thumbnail source={require('../bgd/Contoh.png')} />
                                        <Body>
                                            <Text>{this.props.auth.token.name}</Text>
                                            <Text note>{this.props.user.users.profiles}</Text>
                                        </Body>
                                    </Left>
                                    <Right>
                                        <Button
                                            full
                                            onPress={() => this.logout()}
                                        >
                                            <Text>Logout</Text>
                                        </Button>
                                    </Right>
                                </CardItem>
                            </Card>
                            <Tabs>
                                <Tab heading="History">
                                    <History_User />
                                </Tab>
                                <Tab heading="Edit">
                                    <Edit />
                                </Tab>
                            </Tabs>
                        </ScrollView>
                    </View>
                </Container>
            )
        } else if (this.props.auth.token.role == "Merchant") {
            return (
                <Maaf />
            )
        } else {
            return (
                <Container>
                    <Header_Login judul={'Profile'} />
                    <View>
                        <Spinner />
                    </View>
                </Container>
            )
        }
    }
}

export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(Profiles))