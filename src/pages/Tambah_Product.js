import React, { Component } from 'react'
import { ToastAndroid, PermissionsAndroid, Modal } from 'react-native'
import { withNavigation } from 'react-navigation'
import { Text, Container, Item, Label, Input, View, Button, Spinner } from 'native-base'
import Header_Login from '../component/Header_Login'
import Axios from 'axios'
import * as functs from '../functions/async'
import { connect } from 'react-redux'
import { auth } from '../redux/action/AuthAction';
import { user } from '../redux/action/UserAction'
import ImagePicker from 'react-native-image-picker'

const mapStateToProps = state => ({
    auth: state.auth,
    user: state.user
});

const mapDispatchToProps = (dispatch) => {
    return {
        FetchToken: token => dispatch(auth(token)),
        FetchUser: users => dispatch(user(users))
    };
};

class Tambah_Product extends Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            price: '',
            stock: '',
            desc: '',
            id: '',
            photo: null,
            active: false
        }
    }

    selectGambar = () => {
        const options = {
            title: 'Select Avatar',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };

        ImagePicker.showImagePicker(options, (response) => {
            this.setState({
                photo: response
            })

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else {
                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };

                this.setState({
                    photo: response
                })
            }
        });
    }

    Tambah = async () => {
        const token = await functs.getData(`token`)
        try {
            const add = async (data) => await Axios.post(
                `http://basic-commerce.herokuapp.com/api/product/create/`,
                data,
                {
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `jwt ${token}`
                    }
                }
            )

            add({
                name: this.state.name,
                price: this.state.price,
                stock: this.state.stock,
                description: this.state.desc
            })
                .then(res => {
                    this.setState({
                        id: res.data.result._id
                    })
                    this.foto()
                })
                .catch(err => {
                    console.log(err)
                })
        } catch (e) {
            console.log(e)
        }
    }

    foto = async () => {
        const token = await functs.getData(`token`)
        try {
            console.log(this.state.photo.fileName)
            let photo = new FormData()
            photo.append('preview',
                {
                    uri: this.state.photo.uri,
                    name: this.state.photo.fileName,
                    type: this.state.photo.type
                }
            )

            console.log(photo)

            const add = async (data) => await Axios.post(
                `https://basic-commerce.herokuapp.com/api/upload/${this.state.id}`,
                data,
                {
                    headers: {
                        'Content-Type': 'multipart/form-data',
                        'Authorization': `jwt ${token}`
                    }
                }
            )

            add(photo)
                .then(res => {
                    console.log(res)
                    this.setState({
                        active: false
                    })
                    this.props.navigation.pop()
                })
                .catch(err => {
                    console.log(err)
                    this.props.navigation.pop()
                })
        } catch (e) {
            console.log(e)
        }
    }

    render() {
        return (
            <Container>
                <Header_Login judul={'Tambah'} />
                <View
                    style={{
                        display: 'flex',
                        flex: 1,
                        justifyContent: 'center'
                    }}
                >
                    <Item
                        floatingLabel
                        style={{
                            width: '60%',
                            alignSelf: 'center',
                        }}
                    >
                        <Label>
                            <Text>Nama Produk</Text>
                        </Label>
                        <Input
                            keyboardType='default'
                            secureTextEntry={false}
                            onChangeText={(text) => {
                                this.setState({
                                    name: text
                                })
                            }}
                        />
                    </Item>
                    <Item
                        floatingLabel
                        style={{
                            width: '60%',
                            alignSelf: 'center',
                        }}
                    >
                        <Label>
                            <Text>Harga</Text>
                        </Label>
                        <Input
                            keyboardType='default'
                            secureTextEntry={false}
                            onChangeText={(text) => {
                                this.setState({
                                    price: text
                                })
                            }}
                        />
                    </Item>
                    <Item
                        floatingLabel
                        style={{
                            width: '60%',
                            alignSelf: 'center',
                        }}
                    >
                        <Label>
                            <Text>Stok</Text>
                        </Label>
                        <Input
                            keyboardType='default'
                            secureTextEntry={false}
                            onChangeText={(text) => {
                                this.setState({
                                    stock: text
                                })
                            }}
                        />
                    </Item>
                    <Item
                        floatingLabel
                        style={{
                            width: '60%',
                            alignSelf: 'center',
                        }}
                    >
                        <Label>
                            <Text>Deskripsi</Text>
                        </Label>
                        <Input
                            keyboardType='default'
                            secureTextEntry={false}
                            onChangeText={(text) => {
                                this.setState({
                                    desc: text
                                })
                            }}
                        />
                    </Item>
                    <Button
                        full
                        style={{
                            width: '60%',
                            alignSelf: 'center'
                        }}
                        onPress={() => this.selectGambar()}
                    >
                        <Text>Select Gambar :(</Text>
                    </Button>
                    <Button
                        full
                        style={{
                            width: '60%',
                            alignSelf: 'center'
                        }}
                        onPress={() => {
                            this.setState({
                                active: true
                            })
                            this.Tambah()
                        }}
                    >
                        <Text>Tambah!</Text>
                    </Button>
                </View>
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.active}
                    onRequestClose={
                        () => Alert.alert(`Hapus Berhasil`)
                    }
                >
                    <View
                        style={{
                            backgroundColor: 'rgba(0,0,0,0.4)',
                            display: 'flex',
                            flex: 1,
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}
                    >
                        <Spinner />
                    </View>
                </Modal>
            </Container >
        )
    }
}
export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(Tambah_Product))