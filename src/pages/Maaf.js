import React, { Component } from 'react'
import { withNavigation } from 'react-navigation'
import { Container, View, Text, Button } from 'native-base'
import Header_Login from '../component/Header_Login'
import { ScrollView } from 'react-native-gesture-handler'
import * as functs from '../functions/async'

class Maaf extends Component {

    logout = () => {
        functs.setData('token', 'null')
        functs.setData('username', 'null')
        this.props.navigation.navigate('Home')
    }

    render() {
        return (
            <Container>
                <Header_Login judul={'Salah'} />
                <View>
                    <ScrollView>
                        <Text>Maaf Fitur Tidak untuk kamu sayang :*</Text>
                        <Button
                            full
                            onPress={() => this.logout()}
                        >
                            <Text>Logout</Text>
                        </Button>
                    </ScrollView>
                </View>
            </Container>
        )
    }
}

export default withNavigation(Maaf)