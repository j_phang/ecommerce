import React, { Component } from 'react'
import { View, ToastAndroid, Modal } from 'react-native'
import { withNavigation } from 'react-navigation'
import { Text, Container, Item, Label, Input, Button, DatePicker, Toast, Form, Picker, Spinner } from 'native-base'
import Header_Login from '../component/Header_Login'
import Axios from 'axios'
import { Dropdown } from 'react-native-material-dropdown';

class Register extends Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            email: '',
            password: '',
            name: '',
            role: 'Buyer',
            tags: 'Buyer',
            active: false
        }
    }

    getToken = async => {
        try {
            const Token = async (data) => await Axios.post(
                `https://basic-commerce.herokuapp.com/api/user/login`,
                data
            )

            Token({
                'username': this.state.username,
                'password': this.state.password
            })
                .then(res => {
                    console.log(`Bisa ambil token`)
                    this.MerchantBuyer(res.data.result.token)
                })
                .catch(err => {
                    console.log(err.message)
                })
        }
        catch (err) {
            console.log(err)
        }
    }

    MerchantBuyer = async (token) => {
        console.log(`Merchant Buyer dengan token ${token}`)
        try {
            const Daftar = async (data) => await Axios.post(
                `https://basic-commerce.herokuapp.com/api/profile/create`, data, {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `jwt ${token}`
                }
            }
            )
            Daftar({
                'name': this.state.name,
                'role': this.state.role,
                'tags': this.state.tags
            })
                .then(res => {
                    console.log('Berhasil buat akun sih')
                    ToastAndroid.show('Berhasil buat akun!', ToastAndroid.SHORT)
                    this.setState({
                        active: false
                    })
                    this.props.navigation.pop()
                })
                .catch(err => {
                    console.log(err.message)
                })
        }
        catch (err) {
            console.log(err.message)
        }
    }

    register = async => {
        try {
            const Daftar = async (data) => await Axios.post(
                `https://basic-commerce.herokuapp.com/api/user/create`,
                data,
                {
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }
            )

            Daftar({
                'username': this.state.username,
                'email': this.state.email,
                'password': this.state.password
            })
                .then(res => {
                    this.getToken()
                })
                .catch(e => {
                    console.log(e.message)
                })
        }
        catch (e) {
            console.log(e.message)
        }
    }
    render() {
        if (this.state.role == 'Merchant') {
            return (
                <Container>
                    <Header_Login />
                    <View
                        style={{
                            display: 'flex',
                            flex: 1,
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}
                    >
                        <Item
                            floatingLabel
                            style={{
                                width: '60%',
                                alignSelf: 'center',
                            }}
                        >
                            <Label>
                                <Text>Username</Text>
                            </Label>
                            <Input
                                onChangeText={(text) => {
                                    this.setState({
                                        username: text
                                    })
                                }}
                            />
                        </Item>
                        <Item
                            floatingLabel
                            style={{
                                width: '60%',
                                alignSelf: 'center',
                            }}
                        >
                            <Label>
                                <Text>Email!</Text>
                            </Label>
                            <Input
                                keyboardType='email-address'
                                onChangeText={(text) => {
                                    this.setState({
                                        email: text
                                    })
                                }}
                            />
                        </Item>
                        <Item
                            floatingLabel
                            style={{
                                width: '60%',
                                alignSelf: 'center',
                            }}
                        >
                            <Label>
                                <Text>Password</Text>
                            </Label>
                            <Input
                                keyboardType='default'
                                secureTextEntry={true}
                                onChangeText={(text) => {
                                    this.setState({
                                        password: text
                                    })
                                }}
                            />
                        </Item>
                        <Item
                            floatingLabel
                            style={{
                                width: '60%',
                                alignSelf: 'center',
                            }}
                        >
                            <Label>
                                <Text>Nama Toko</Text>
                            </Label>
                            <Input
                                keyboardType='default'
                                secureTextEntry={false}
                                onChangeText={(text) => {
                                    this.setState({
                                        name: text
                                    })
                                }}
                            />
                        </Item>
                        <Item
                            floatingLabel
                            style={{
                                width: '60%',
                                alignSelf: 'center',
                            }}
                        >
                            <Label>
                                <Text>Tags</Text>
                            </Label>
                            <Input
                                keyboardType='default'
                                secureTextEntry={false}
                                onChangeText={(text) => {
                                    this.setState({
                                        tags: text
                                    })
                                }}
                            />
                        </Item>
                        <Item
                            style={{
                                width: '60%',
                                alignSelf: 'center',
                            }}
                        >
                            <Picker
                                note
                                mode="dropdown"
                                style={{ width: 120 }}
                                selectedValue={this.state.role}
                                onValueChange={
                                    (text) => this.setState({
                                        role: text
                                    })
                                }
                            >
                                <Picker.Item label="Merchant" value="Merchant" />
                                <Picker.Item label="Buyer" value="Buyer" />
                            </Picker>
                        </Item>
                        <Button
                            full
                            style={{
                                alignSelf: 'center',
                                width: '60%',
                            }}
                            onPress={() => {
                                this.setState({
                                    active: true
                                })
                                this.register()
                            }}
                        >
                            <Text>
                                Daftar!
                            </Text>
                        </Button>
                    </View>
                    <Modal
                        animationType="fade"
                        transparent={true}
                        visible={this.state.active}
                        onRequestClose={
                            () => Alert.alert(`Hapus Berhasil`)
                        }
                    >
                        <View
                            style={{
                                backgroundColor: 'rgba(0,0,0,0.4)',
                                display: 'flex',
                                flex: 1,
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}
                        >
                            <Spinner />
                        </View>
                    </Modal>
                </Container>
            )
        } else {
            return (
                <Container>
                    <Header_Login />
                    <View
                        style={{
                            display: 'flex',
                            flex: 1,
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}
                    >
                        <Item
                            floatingLabel
                            style={{
                                width: '60%',
                                alignSelf: 'center',
                            }}
                        >
                            <Label>
                                <Text>Username</Text>
                            </Label>
                            <Input
                                onChangeText={(text) => {
                                    this.setState({
                                        username: text
                                    })
                                }}
                            />
                        </Item>
                        <Item
                            floatingLabel
                            style={{
                                width: '60%',
                                alignSelf: 'center',
                            }}
                        >
                            <Label>
                                <Text>Email!</Text>
                            </Label>
                            <Input
                                keyboardType='email-address'
                                onChangeText={(text) => {
                                    this.setState({
                                        email: text
                                    })
                                }}
                            />
                        </Item>
                        <Item
                            floatingLabel
                            style={{
                                width: '60%',
                                alignSelf: 'center',
                            }}
                        >
                            <Label>
                                <Text>Nama</Text>
                            </Label>
                            <Input
                                keyboardType='default'
                                secureTextEntry={false}
                                onChangeText={(text) => {
                                    this.setState({
                                        name: text
                                    })
                                }}
                            />
                        </Item>
                        <Item
                            floatingLabel
                            style={{
                                width: '60%',
                                alignSelf: 'center',
                            }}
                        >
                            <Label>
                                <Text>Password</Text>
                            </Label>
                            <Input
                                keyboardType='default'
                                secureTextEntry={true}
                                onChangeText={(text) => {
                                    this.setState({
                                        password: text
                                    })
                                }}
                            />
                        </Item>
                        <Item
                            style={{
                                width: '60%',
                                alignSelf: 'center',
                            }}
                        >
                            <Picker
                                note
                                mode="dropdown"
                                style={{ width: 120 }}
                                selectedValue={this.state.role}
                                onValueChange={
                                    (text) => this.setState({
                                        role: text
                                    })
                                }
                            >
                                <Picker.Item label="Merchant" value="Merchant" />
                                <Picker.Item label="Buyer" value="Buyer" />
                            </Picker>
                        </Item>
                        <Button
                            full
                            style={{
                                alignSelf: 'center',
                                width: '60%',
                            }}
                            onPress={() => {
                                this.setState({
                                    active: true
                                })
                                this.register()
                            }}
                        >
                            <Text>
                                Daftar!
                            </Text>
                        </Button>
                    </View>
                    <Modal
                        animationType="fade"
                        transparent={true}
                        visible={this.state.active}
                        onRequestClose={
                            () => Alert.alert(`Hapus Berhasil`)
                        }
                    >
                        <View
                            style={{
                                backgroundColor: 'rgba(0,0,0,0.4)',
                                display: 'flex',
                                flex: 1,
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}
                        >
                            <Spinner />
                        </View>
                    </Modal>
                </Container>
            )
        }
    }
}

export default withNavigation(Register)