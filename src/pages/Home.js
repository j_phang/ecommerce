import React, { Component } from 'react'
import { Dimensions, Image, ImageBackground, ToastAndroid, Modal } from 'react-native'
import { withNavigation } from 'react-navigation'
import { Text, Container, Card, CardItem, Left, Thumbnail, Body, Right, Button, Fab, Icon, Header, Spinner, View } from 'native-base'
import Headeres from '../component/Header_Home';
import { ScrollView } from 'react-native-gesture-handler';
import Axios from 'axios';
import * as functs from '../functions/async'
import { connect } from 'react-redux'
import { auth } from '../redux/action/AuthAction';
import { user } from '../redux/action/UserAction'
import { barang } from '../redux/action/BarangAction'

const mapStateToProps = state => ({
    auth: state.auth,
    user: state.user,
    barang: state.barang
});

const mapDispatchToProps = (dispatch) => {
    return {
        FetchToken: token => dispatch(auth(token)),
        FetchUser: users => dispatch(user(users)),
        Barang: id => dispatch(barang(id))
    };
};

class Home extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [],
            token: 'null',
            loading: true,
            contoh: [],
            active: false

        }
    }

    componentDidMount() {
        this.getToken()
        this.setState({
            loading: true
        })
        setInterval(this.Jalan, 1000)
        setInterval(this.fetchData, 1000)
        // this.Jalan()
        // this.fetchData()
    }

    getToken = async () => {
        const tokenn = await functs.getData('token')
        this.setState({
            token: tokenn
        })
    }

    fetchData = async () => {
        try {
            const dash = async () => await Axios.get(
                `http://basic-commerce.herokuapp.com/api/product/list`
            )

            dash()
                .then(res => {
                    // console.log(res.data.result)
                    this.setState({
                        data: res.data.result
                    })

                    this.setState({
                        loading: false
                    })
                })
                .catch(err => {
                    console.log(err)
                })
        } catch (err) {
            console.log(err)
        }
    }

    Jalan = async () => {
        const tokens = await functs.getData(`token`)
        this.setState({
            token: tokens
        })
        if (tokens !== 'null') {
            this.getProfileId()
        } else {
            console.log('Token kosong sayang')
        }
    }

    getProfile = async (id) => {
        const token = await functs.getData(`token`)
        try {
            const getProfile = async () => Axios.get(
                `https://basic-commerce.herokuapp.com/api/profile/show/${id}`,
                {
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `jwt ${token}`
                    }
                }
            )

            getProfile()
                .then(
                    res => {
                        // console.log(`ini getProfile`)
                        // console.log(this.state.token)
                        this.props.FetchToken(res.data.result)
                    }
                )
                .catch(
                    err => {
                        console.log(res)
                    }
                )
        } catch (err) {
            console.log(err)
        }
    }

    getProfileId = async () => {
        const token = await functs.getData(`token`)
        try {
            const getRoles = async () => Axios.get(
                `http://basic-commerce.herokuapp.com/api/user/info`,
                {
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `jwt ${token}`
                    }
                }
            )

            getRoles()
                .then(
                    res => {
                        // console.log(`ini getProfileId`)
                        this.props.FetchUser(res.data.result)
                        this.getProfile(res.data.result.profiles[0])
                    }
                )
                .catch(
                    err => {
                        functs.setData('token', 'null')
                        console.log(err)
                    }
                )
        } catch (e) {
            console.log(e.message)
        }
    }

    addCart = async (product) => {
        const token = await functs.getData('token')
        try {
            const api = async (objParam) => await Axios.post(
                `http://basic-commerce.herokuapp.com/api/order/add-product/${this.props.auth.token.orders[0]}`,
                objParam,
                {
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `jwt ${token}`
                    }
                }
            )

            api({
                amount: 1,
                products: product
            })
                .then(res => {
                    console.log(res)
                    ToastAndroid.show('Dimasukkan ke keranjang', ToastAndroid.SHORT)
                    this.setState({
                        active: false
                    })
                })
                .catch(err => {
                    ToastAndroid.show('Sedang Membuat Cart', ToastAndroid.SHORT)
                    this.createCart()
                    console.log(err.message)
                })
        } catch (err) {
            console.log(err)
        }
    }

    createCart = async () => {
        const token = await functs.getData('token')
        try {
            const api = async (objParam) => await Axios.post(
                `http://basic-commerce.herokuapp.com/api/order/create`,
                objParam,
                {
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `jwt ${token}`
                    }
                }
            )

            api()
                .then(res => {
                    console.log(res)
                    this.setState({
                        active: false
                    })
                    ToastAndroid.show(`Cart Berhasil dibuat! Silahkan masukkan Barang`, ToastAndroid.SHORT)
                })
                .catch(err => {
                    console.log(err)
                })
        } catch (err) {
            console.log(err)
        }
    }

    render() {
        // console.log(this.props.user)
        // console.log(this.props.auth)
        // console.log(this.state.data)
        const loops = this.state.data.map(value => {
            return (
                <Card
                    key={value._id}
                >
                    <CardItem button
                        onPress={
                            () => {
                                this.props.Barang(value._id)
                                this.props.navigation.navigate('Details')
                            }
                        }
                    >
                        <Left>
                            <Thumbnail source={require('../bgd/Contoh.png')} />
                            <Body>
                                <Text>{value.name}</Text>
                                <Text note>Stok: {value.stock}</Text>
                            </Body>
                        </Left>
                    </CardItem>
                    <CardItem cardBody button
                        onPress={
                            () => {
                                this.props.Barang(value._id)
                                /* 1. Navigate to the Details route with params */
                                this.props.navigation.navigate('Details');
                            }
                        }
                    >
                        <Image
                            source={{ uri: value.preview }}
                            style={{ height: 200, width: null, flex: 1 }}
                        />
                    </CardItem>
                    <CardItem>
                        <Left>
                            <Text>
                                Rp. {value.price}
                            </Text>
                        </Left>
                        <Right>
                            <Button
                                rounded
                                onPress={
                                    () => {
                                        this.setState({
                                            active: true
                                        })

                                        if (this.props.auth.token.role !== 'Buyer') {
                                            ToastAndroid.show('Maaf Tidak bisa malakukan ini', ToastAndroid.SHORT)
                                        } else if (this.props.auth.token.orders !== this.state.contoh) {
                                            this.addCart(value._id)
                                        } else {
                                            console.log('Ini')
                                            this.createCart()
                                        }
                                    }
                                }
                            >
                                <Text>
                                    Beli Sekarang!
                            </Text>
                            </Button>
                        </Right>
                    </CardItem>
                </Card>
            )
        })
        if (this.state.loading) {
            return (
                <Container>
                    <Header transparent />
                    <View>
                        <Spinner />
                    </View>
                </Container>
            )
        } else {
            return (
                <Container>
                    <ImageBackground
                        source={require('../bgd/Home.jpg')}
                        style={{
                            width: Dimensions.get('window').width,
                            height: Dimensions.get('window').height
                        }}
                    >
                        <Headeres navigasi={this.props.navigation.navigate} />
                        <ScrollView>
                            {loops}
                        </ScrollView>
                        <Modal
                            animationType="fade"
                            transparent={true}
                            visible={this.state.active}
                            onRequestClose={
                                () => Alert.alert(`Hapus Berhasil`)
                            }
                        >
                            <View
                                style={{
                                    backgroundColor: 'rgba(0,0,0,0.4)',
                                    display: 'flex',
                                    flex: 1,
                                    justifyContent: 'center',
                                    alignItems: 'center'
                                }}
                            >
                                <Spinner />
                            </View>
                        </Modal>
                    </ImageBackground>
                </Container>
            )
        }
    }
}

export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(Home))