import React, { Component } from 'react'
import { Text, View, Dimensions, ImageBackground, Modal } from 'react-native'
import { withNavigation } from 'react-navigation'
import { Button, Container, Item, Label, Input, Content, Footer, Spinner } from 'native-base'
import Header_Login from '../component/Header_Login'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler'
import Axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import * as functs from '../functions/async'
import { connect } from 'react-redux'
import { auth } from '../redux/action/AuthAction';
import { user } from '../redux/action/UserAction'

const mapStateToProps = state => ({
    auth: state.auth,
    user: state.user
});
// redux store in here

const mapDispatchToProps = dispatch => {
    return {
        FetchToken: token => dispatch(auth(token)),
        FetchUser: users => dispatch(user(users))
    };
};

class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            password: '',
            active: false
        }
    }

    componentDidMount() {
        console.log(this.props.auth)
        console.log(this.props.user)
    }

    login = async () => {
        const masuk = async (data) => await Axios.post(
            `https://basic-commerce.herokuapp.com/api/user/login`, data
        )
        masuk({
            username: this.state.username,
            password: this.state.password
        })
            .then(res => {
                functs.setData(`token`, res.data.result.token)
                this.setState({
                    active: false
                })
                this.props.navigation.pop()
            })
            .catch(err => {
                console.log(err.message)
            })
    }

    render() {
        return (
            <Container>
                <ImageBackground
                    source={require('../bgd/Login.jpg')}
                    style={{
                        width: Dimensions.get('window').width,
                        height: Dimensions.get('window').height
                    }}>
                    <Header_Login judul={'Login'} />
                    <View
                        style={{
                            display: 'flex',
                            flex: 1,
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}
                    >
                        <Item
                            floatingLabel
                            style={{
                                width: '60%',
                                alignSelf: 'center',
                            }}
                        >
                            <Label>
                                <Text>Username!</Text>
                            </Label>
                            <Input
                                onChangeText={(text) => {
                                    this.setState({
                                        username: text
                                    })
                                }}
                            />
                        </Item>
                        <Item
                            floatingLabel
                            style={{
                                width: '60%',
                                alignSelf: 'center',
                            }}
                        >
                            <Label>
                                <Text>Password!</Text>
                            </Label>
                            <Input
                                secureTextEntry={true}
                                onChangeText={(text) => {
                                    this.setState({
                                        password: text
                                    })
                                }}
                            />
                        </Item>
                        <Button
                            full
                            success
                            style={{
                                alignSelf: 'center',
                                width: '60%',
                            }}
                            onPress={
                                () => {
                                    this.setState({
                                        active: true
                                    })
                                    this.login()
                                }
                            }
                        >
                            <Text>
                                Login!
                            </Text>
                        </Button>
                    </View>
                    <Button
                        full
                        transparent
                        style={{
                            display: 'flex',
                            alignSelf: 'center',
                            width: Dimensions.get('window').width
                        }}
                        onPress={() => {
                            this.props.navigation.navigate('Register')
                        }}
                    >
                        <Text>Belum daftar? Klik disini!</Text>
                    </Button>
                    <Modal
                        animationType='fade'
                        transparent={true}
                        visible={this.state.active}
                        onRequestClose={
                            () => Alert.alert(`Hapus Berhasil`)
                        }
                    >
                        <View
                            style={{
                                backgroundColor: 'rgba(0,0,0,0.4)',
                                display: 'flex',
                                flex: 1,
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}
                        >
                            <Spinner />
                        </View>
                    </Modal>
                </ImageBackground>
            </Container >
        )
    }
}

export default withNavigation(connect(mapDispatchToProps, mapStateToProps)(Login))