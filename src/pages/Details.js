import React, { Component } from 'react'
import { Image, ToastAndroid, ImageBackground, ScrollView } from 'react-native'
import { withNavigation } from 'react-navigation'
import { Text, Container, Card, CardItem, Left, Body, Right, Button, Footer, FooterTab, Spinner, View, Header } from 'native-base'
import Header_Login from '../component/Header_Login'
import * as functs from '../functions/async'
import { connect } from 'react-redux'
import { auth } from '../redux/action/AuthAction';
import { user } from '../redux/action/UserAction'
import { barang } from '../redux/action/BarangAction'
import Axios from 'axios';

const mapStateToProps = state => ({
    auth: state.auth,
    user: state.user,
    barang: state.barang
});

const mapDispatchToProps = (dispatch) => {
    return {
        FetchToken: token => dispatch(auth(token)),
        FetchUser: users => dispatch(user(users)),
        FetchBarang: id => dispatch(barang(id))
    };
};
class Details extends Component {

    constructor(props) {
        super(props)
        this.state = {
            details: null,
            loading: true,
            contoh: []
        }
    }

    componentDidMount() {
        console.log(this.props.barang.barang)
        this.getDetails()
    }

    getDetails = async () => {
        try {
            console.log('Sudah sampai sini')
            const Details = async () => await Axios.get(
                `https://basic-commerce.herokuapp.com/api/product/detail/${this.props.barang.barang}`,
                {
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }
            )

            Details()
                .then(res => {
                    console.log('Sudah sampai then')
                    this.setState({
                        details: res.data.result,
                        loading: false
                    })
                    console.log(this.state.details)
                })
                .catch(err => {
                    console.log(err)
                })
        } catch (err) {
            console.log(err)
        }
    }

    addCart = async (product) => {
        const token = await functs.getData('token')
        try {
            const api = async (objParam) => await Axios.post(
                `http://basic-commerce.herokuapp.com/api/order/add-product/${this.props.auth.token.orders[0]}`, objParam, {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `jwt ${token}`
                }
            }

            )

            api({
                amount: 1,
                products: product
            })
                .then(res => {
                    console.log(res)
                })
                .catch(err => {
                    console.log(err)
                })
        } catch (err) {
            console.log(err)
        }
    }

    render() {
        if (this.state.loading) {
            return (
                <Container>
                    <Header transparent />
                    <View>
                        <Spinner />
                    </View>
                </Container>
            )
        } else {
            return (
                <Container>
                    <Header_Login judul={'Details'} />
                    <ScrollView>
                        <Card>
                            <CardItem cardBody>
                                <Image
                                    source={{ uri: this.state.details.preview }}
                                    style={{ height: 200, width: null, flex: 1 }}
                                />
                            </CardItem>
                            <CardItem bordered>
                                <Text>{this.state.details.name}</Text>
                                {/* <Text note>Nama Barang</Text> */}
                            </CardItem>
                            <CardItem>
                                <Text> Jumlah Stok : {this.state.details.stock} </Text>
                            </CardItem>
                        </Card>
                        <Card>
                            <CardItem>
                                <Text>
                                    Deskripsi Produk
                                </Text>
                            </CardItem>
                            <CardItem>
                                <Text>
                                    Deskripsi dan Harusnya ini panjang banget
                                    pakai banget ya jangan lupa, soalnya itu
                                    beda banget kalau ga pakai banget. Setelah
                                    deskripsi biasanya bacot bacot penjual biar
                                    barangnya laku gaes
                                </Text>
                            </CardItem>
                        </Card>
                    </ScrollView>
                    <Footer>
                        <FooterTab
                            style={{
                                backgroundColor: 'white'
                            }}
                        >
                            <Left
                                style={{
                                    marginBottom: '5%',
                                    marginLeft: '5%'
                                }}
                            >
                                <Text>
                                    Rp. {this.state.details.price}
                                </Text>
                            </Left>
                            <Right
                                style={{
                                    marginBottom: '5%',
                                    marginRight: '5%'
                                }}
                            >
                                <Button
                                    success
                                    rounded
                                    onPress={
                                        () => {
                                            console.log(this.props.auth.token.role)
                                            if (this.props.auth.token.role !== 'Buyer') {
                                                ToastAndroid.show('Maaf Tidak bisa malakukan ini', ToastAndroid.SHORT)
                                            } else if (this.props.auth.token.orders !== this.state.contoh) {
                                                this.addCart(this.state.details._id)
                                                ToastAndroid.show('Dimasukkan ke keranjang', ToastAndroid.SHORT)
                                            } else {
                                                this.createCart()
                                            }
                                        }
                                    }
                                >
                                    <Text>
                                        Beli Sekarang!
                                    </Text>
                                </Button>
                            </Right>
                        </FooterTab>
                    </Footer>
                </Container>
            )
        }
    }
}
export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(Details))