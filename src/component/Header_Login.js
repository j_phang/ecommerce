import React, { Component } from 'react'
import { withNavigation } from 'react-navigation'
import { Container, Header, Left, Body, Right, Button, Icon, Title, Text } from 'native-base';

class Header_Login extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        return (
            <Header
                transparent
            >
                <Left
                    style={{
                        display: 'flex',
                        flexDirection: 'row'
                    }}
                >
                    <Icon
                        type="MaterialIcons"
                        name="arrow-back"
                        style={{
                            color: 'black'
                        }}
                        onPress={
                            () => {
                                this.props.navigation.pop()
                            }
                        }
                    />
                </Left>
                <Body>
                    <Title
                        style={{
                            color: `black`
                        }}
                    >
                        {this.props.judul}
                    </Title>
                </Body>
                <Right />
            </Header>
        )
    }
}

export default withNavigation(Header_Login)