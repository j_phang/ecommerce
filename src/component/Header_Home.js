import React, { Component } from 'react'
import { Text, View, Dimensions, ToastAndroid } from 'react-native'
import { Left, Button, Icon, Header, Body, Title, Item, Label, Input, Right } from 'native-base'
import { withNavigation } from 'react-navigation'
import * as functs from '../functions/async'
import { connect } from 'react-redux'
import { auth } from '../redux/action/AuthAction';
import { user } from '../redux/action/UserAction'
import { barang } from '../redux/action/BarangAction'

const mapStateToProps = state => ({
    auth: state.auth,
    user: state.user,
    barang: state.barang
});

const mapDispatchToProps = (dispatch) => {
    return {
        FetchToken: token => dispatch(auth(token)),
        FetchUser: users => dispatch(user(users)),
        Barang: id => dispatch(barang(id))
    };
}

class Headeres extends Component {
    render() {
        return (
            <Header
                transparent
            >
                <Body
                    style={{
                        display: 'flex',
                        flex: 1,
                        flexDirection: 'row'
                    }}
                >
                    <Title
                        style={{
                            color: 'black'
                        }}
                    >
                        HomePage
                    </Title>
                </Body>
                <Right>
                    <Icon
                        type="FontAwesome5"
                        name="shopping-cart"
                        style={{
                            fontSize: 30,
                            marginHorizontal: 6
                        }}
                        onPress={
                            () => {
                                if (this.props.auth.token.role == 'Merchant') {
                                    ToastAndroid.show('Maaf Tidak bisa malakukan ini', ToastAndroid.SHORT)
                                } else if (this.props.auth.token.orders[0] == null) {
                                    //create Cart
                                    console.log(`ga ada cart`)
                                    ToastAndroid.show('Maaf belum buat Cart sayang :*', ToastAndroid.SHORT)
                                } else {
                                    //masuk ke keranjang
                                    console.log(`ada cart`)
                                    this.props.navigasi('Cart')
                                }
                            }
                        }
                    />
                    <Icon
                        type="FontAwesome5"
                        name="user"
                        style={{
                            fontSize: 30,
                            marginHorizontal: 6
                        }}
                        onPress={
                            () => {
                                this.props.navigasi('Profile')
                            }
                        }
                    />
                    <Icon
                        type="Entypo"
                        name="shop"
                        style={{
                            fontSize: 30,
                            marginHorizontal: 6
                        }}
                        onPress={
                            () => {
                                this.props.navigasi('Store')
                            }
                        }
                    />
                </Right>
            </Header>
        )
    }
}

export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(Headeres))